$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("/Users/UserQV/Documents/taller cucumber/taller-cucumber-master/src/test/resources/features/1_DEM-1.feature");
formatter.feature({
  "line": 2,
  "name": "test google",
  "description": "",
  "id": "test-google",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@DEM-2"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "Verify open google",
  "description": "",
  "id": "test-google;verify-open-google",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@TEST_DEM-1"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I launch Chrome browser",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I open Google Homepage",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I verify that the page displays search text box",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "the page displays Google Search button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "the page displays Im Feeling Lucky button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "the browser close",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinition_Google.i_launch_Chrome_browserr()"
});
formatter.result({
  "duration": 8497705300,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_Google.i_open_Google_Homepage()"
});
formatter.result({
  "duration": 1539337700,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_Google.i_verify_that_the_page_displays_search_text_box()"
});
formatter.result({
  "duration": 107104100,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_Google.the_page_displays_Google_Search_button()"
});
formatter.result({
  "duration": 41923600,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_Google.the_page_displays_Im_Feeling_Lucky_button()"
});
formatter.result({
  "duration": 54812100,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_Google.the_browser_close()"
});
formatter.result({
  "duration": 1450713900,
  "status": "passed"
});
});