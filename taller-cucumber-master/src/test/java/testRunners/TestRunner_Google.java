package testRunners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
		features="\\Users\\UserQV\\Documents\\taller cucumber\\taller-cucumber-master\\src\\test\\resources\\features\\1_DEM-1.feature",
		glue={"stepDefinitions"},
		format =  {"pretty","html:test-output","json:json_output/cucumber.json","junit:junit_xml/cucumber.xml"},
		monochrome = true,
		strict = true,
		dryRun = false
		)

public class TestRunner_Google {
 
}
